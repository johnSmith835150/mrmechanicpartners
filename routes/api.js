const express = require('express');
const router = express.Router();
const multer = require("multer");

const mechanicApiController = require("../app/controllers/api/mechanic");
const storeApiController = require("../app/controllers/api/store");
const craneApiController = require("../app/controllers/api/crane");

const upload = multer({dest:"uploads/"});

// auth routes
router.post('/getMechanic', mechanicApiController.getMechanic);
router.post('/getMechanicReviews', mechanicApiController.getReviews);
router.post('/addMechanicReview', mechanicApiController.addReview);
router.get('/getMechanics', mechanicApiController.getMechanics);

router.get('/getStores', storeApiController.getStores);
router.get('/getCranes', craneApiController.getCranes);

module.exports = router;
