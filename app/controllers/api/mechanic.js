const utils = require('../../utils/util');

var exports = module.exports = {};
exports.Mechanic = null;
exports.MechanicReview = null;

exports.getMechanics = async (req, res) => {
    let mechanics = [];
    if (req.query.manufacturer)
        mechanics = await exports.Mechanic.find({ manufacturers: req.query.manufacturer }).select('latitude longitude verified');
    else
        mechanics = await exports.Mechanic.find({}).select('latitude longitude verified');

    res.json({mechanics: mechanics});
}

exports.getMechanic = async (req, res) => {
    req.checkBody('id', 'id must be provided').notEmpty();
    var err = req.validationErrors();
    var errors = [];

    if (err || errors.length) {
        if (err) {
            err.forEach(element => {
                errors.push(element.msg);
            });
            errors = [...new Set(errors)];
        }

        for (var i = 0; i < errors.length; i++)
            errors[i] = { message: errors[i], code: 50 };

        res.json({ status: false, message: "Mechanic data was retrived unsuccessful", errors: errors });
    }
    else {
        let mechanic = await exports.Mechanic.findOne({_id: req.body.id}).select('image_path name email address mobile rate services -_id');
        if (mechanic) {
            res.json({mechanic: mechanic});
        }
        else
            res.sendStatus(403);
    }
}

exports.getReviews = async (req, res) => {
    req.checkBody('id', 'id must be provided').notEmpty();
    var err = req.validationErrors();
    var errors = [];

    if (err || errors.length) {
        if (err) {
            err.forEach(element => {
                errors.push(element.msg);
            });
            errors = [...new Set(errors)];
        }

        for (var i = 0; i < errors.length; i++)
            errors[i] = { message: errors[i], code: 50 };

        res.json({ status: false, message: "Reviews was retrived unsuccessfully", errors: errors });
    }
    else {
        let reviews = await exports.MechanicReview.find({mechanicId: req.body.id}).select('userId content');
        if (reviews)
            res.json({status: true, message: "Reviews was retrived successfully", reviews: reviews, errors: null});
        else
            res.json({status: true, message: "Reviews was retrived successfully", reviews: [], errors: null});
    }
}

exports.addReview = async (req, res) => {
    req.checkBody('mechanicId', 'Mechanic id must be provided').notEmpty();
    req.checkBody('userId', 'User id must be provided').notEmpty();
    req.checkBody('content', "Review must be between 30 and 500 characters").notEmpty().isLength({min: 30, max: 600});

    var err = req.validationErrors();
    var errors = [];

    if (err || errors.length) {
        if (err) {
            err.forEach(element => {
                errors.push(element.msg);
            });
            errors = [...new Set(errors)];
        }

        for (var i = 0; i < errors.length; i++)
            errors[i] = { message: errors[i], code: 50 };

        res.json({ status: false, message: "Review was unsuccessfull", reviews: null, errors: errors });
    }
    else {
        let review = new exports.MechanicReview({
            userId: req.body.userId,
            mechanicId: req.body.mechanicId,
            content: req.body.content,
            createdAt: utils.now(),
            updatedAt: utils.now()
        });

        let savedReview = await review.save();
        if (savedReview)
            res.json({ status: true, message: "Reviews was created successfully", errors: null });
        else
            res.json({ status: false, message: "Review was created unsuccessfully", errors: [
                { message: "Failed to create review, please try again", code: 110 }
            ]});
    }
}
