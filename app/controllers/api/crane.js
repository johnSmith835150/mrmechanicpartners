var exports = module.exports = {};
exports.Crane = null;

exports.getCranes = async (req, res) => {
    let limit = 0;
    if (req.query.limit)
        limit = parseInt(Math.round(req.query.limit));

    let cranes = [];
    if (limit != NaN)
        cranes = await exports.Crane.find({}).select('name address mobile').limit(limit);
    else
        cranes = await exports.Crane.find({}).select('name address mobile');

    for (var i = 0; i < cranes.length; i++) {
        var crane = {
            key: cranes[i]._id,
            name: cranes[i].name,
            address: cranes[i].address,
            mobile: cranes[i].mobile,
        };
        cranes[i] = crane;
    }

    res.json({ cranes: cranes });
}
