const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const mailUser = require('../config/keys').mail.user;
const mailPass = require('../config/keys').mail.pass;

var exports = module.exports = {};

exports.isValidPassword = function(userpass, password) {
    return bcrypt.compareSync(password, userpass);
};

exports.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

exports.contains = function(arr1, arr2) {
    for (i = 0; i < arr2.length; i++)
        if(!arr1.includes(arr2[i])) return false;

    return true;
};

exports.usernames2list = function(usernames) {
    var list = [];
    for (i = 0; i < usernames.length; i++)
        list.push(usernames[i].username);

    return list;
};

exports.isValidDate = function(value) {
    if (!value.match(/^\d{4}-\d{2}-\d{2}$/)) return false;
  
    const date = new Date(value);
    if (!date.getTime()) return false;
    
    if (date.toISOString().slice(0, 10) === value) {
        let currentDate = new Date();
        return Number(value.slice(0, 4)) <= (Number(currentDate.getFullYear()) - 18);
    }
    else
        return false;
};

exports.sendEmail = async (receivers, subject, text) => {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: mailUser,
            pass: mailPass
        }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: `"MrMechanic Support" <${mailUser}>`, // sender address
        to: receivers, // list of receivers
        subject: subject, // Subject line
        text: text, // plain text body
        html: `<b>${text}</b>` // html body
    });

    console.log('Message sent: %s', info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
}

exports.now = function() {
    d = new Date();

    let year = d.getFullYear();
    let month = d.getMonth();
    let day = d.getDate();

    if (month < 10)
        month = `0${month}`;
    
    if (day < 10)
        day = `0${day}`;

    return `${year}-${month}-${day}`;
}
