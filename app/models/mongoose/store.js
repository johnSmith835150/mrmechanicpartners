const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const storeSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    address: { type: String, required: true },
    rate: { type: Number, default: 5 },
    mobile: {type: String, required: true },
    manufacturers: [String],
    parts: [],
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true },
    verified: { type: Boolean, default: false },
    createdAt: { type: String, required: true },
    updatedAt: { type: String, required: true }
}, { collection: 'stores' });

module.exports = mongoose.model('Store', storeSchema);
