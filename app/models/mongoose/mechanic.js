const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mechanicSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    address: { type: String, required: true },
    rate: { type: Number, default: 5 },
    mobile: {type: String, required: true },
    image_path: {type: String, default: "uploads/mechanicDefault.png"},
    manufacturers: [String],
    services: [],
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true },
    verified: { type: Boolean, required: true },
    createdAt: { type: String, required: true },
    updatedAt: { type: String, required: true }
}, { collection: 'mechanics' });

module.exports = mongoose.model('Mechanic', mechanicSchema);
