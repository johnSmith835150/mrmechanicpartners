const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const craneSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    address: { type: String, required: true },
    mobile: {type: String, required: true },
    createdAt: { type: String, required: true },
    updatedAt: { type: String, required: true }
}, { collection: 'cranes' });

module.exports = mongoose.model('Crane', craneSchema);
