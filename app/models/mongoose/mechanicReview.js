const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mechanicReviewSchema = new Schema({
    userId: { type: String, required: true },
    mechanicId: { type: String, required: true },
    content: { type: String, required: true },
    createdAt: { type: String, required: true },
    updatedAt: { type: String, required: true }
}, { collection: 'mechanicReviews' });

module.exports = mongoose.model('MechanicReview', mechanicReviewSchema);
