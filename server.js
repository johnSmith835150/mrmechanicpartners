const morgan = require('morgan');
const express = require('express');
const env = require('dotenv').config();
const port = process.env.PORT || 8080;
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');

var app = require('express')();
var http = require('http').Server(app);

// express config
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());

app.use(morgan('combined'));
app.use("/uploads", express.static(__dirname + '/uploads'));

// Models
var models = require("./app/models/mongoose");

// setting models for API
require("./app/controllers/api/mechanic").Mechanic = models.Mechanic;
require("./app/controllers/api/mechanic").MechanicReview = models.MechanicReview;
require("./app/controllers/api/store").Store = models.Store;
require("./app/controllers/api/crane").Crane = models.Crane;

const routes = require('./routes/web');
app.use('/', routes);

http.listen(port, function(){
    console.log('listening on *:' + port);
});
